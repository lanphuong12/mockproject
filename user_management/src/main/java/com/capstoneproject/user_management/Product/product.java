package com.capstoneproject.user_management.Product;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.*;

import com.capstoneproject.user_management.category.category;
import com.capstoneproject.user_management.image_product.image_product;
import com.capstoneproject.user_management.price_product.price_product;
import com.capstoneproject.user_management.supplier.supplier;

@Entity
@Table(name =  "product")
public class product implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_product", nullable = false)
    private int id_product;

    @Column(name = "name_product", length = 200, nullable = false)
    private String name_product;

    @Column(name = "quantity_product", nullable = false)
    private int quantity_product;

    @Column(name = "description_product", length = Integer.MAX_VALUE, nullable = true)
    private String description_product;

    @Column(name = "flag", nullable = false) // 0 là đã hết hàng 1 còn hàng
    private int flag;

    @ManyToOne
    @JoinColumn(name = "id_supplier") // thông qua khóa ngoại id_supplier
    private supplier id_supplier;

    @ManyToMany
    @JoinTable(name = "product_category", joinColumns = @JoinColumn(name = "id_product", referencedColumnName = "id_product"), inverseJoinColumns = @JoinColumn(name = "id_category", referencedColumnName = "id_category"))
    private Collection<category> product_category;

    @OneToOne(cascade = CascadeType.ALL, optional = false)
    @JoinColumn(name = "current_price_product")
    private price_product price;

    @OneToMany(mappedBy = "image", cascade = CascadeType.ALL) // Quan hệ 1-n với đối tượng ở dưới (ảnh) (1 sản phẩm có nhiều ảnh)
    // MapopedBy trỏ tới tên biến image ở trong product.
    private Collection<image_product> image_products;
    
    public int getId_product() {
        return id_product;
    }

    public void setId_product(int id_product) {
        this.id_product = id_product;
    }

    public String getName_product() {
        return name_product;
    }

    public void setName_product(String name_product) {
        this.name_product = name_product;
    }

    public int getQuantity_product() {
        return quantity_product;
    }

    public void setQuantity_product(int quantity_product) {
        this.quantity_product = quantity_product;
    }

    public String getDescription_product() {
        return description_product;
    }

    public void setDescription_product(String description_product) {
        this.description_product = description_product;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public supplier getId_supplier() {
        return id_supplier;
    }

    public void setId_supplier(supplier id_supplier) {
        this.id_supplier = id_supplier;
    }

    public Collection<category> getProduct_category() {
        return product_category;
    }

    public void setProduct_category(Collection<category> product_category) {
        this.product_category = product_category;
    }

    public price_product getPrice() {
        return price;
    }

    public void setPrice(price_product price) {
        this.price = price;
    }

    public Collection<image_product> getImage_products() {
        return image_products;
    }

    public void setImage_products(Collection<image_product> image_products) {
        this.image_products = image_products;
    }

    
}
