package com.capstoneproject.user_management.Product;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/product/")
public class product_controller {
    @Autowired
    product_service p_service;

    @GetMapping("getAllProduct")
    public List<product> getallplace() {
		List<product> arr = p_service.getAll_product();
        if (arr.size() != 0) {
            return arr;
        } else {
            return Collections.emptyList();
        }
    }
}
