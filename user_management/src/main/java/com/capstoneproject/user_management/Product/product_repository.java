package com.capstoneproject.user_management.Product;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface product_repository extends JpaRepository<product, Integer>{

}