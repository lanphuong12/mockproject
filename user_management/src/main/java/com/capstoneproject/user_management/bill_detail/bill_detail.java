package com.capstoneproject.user_management.bill_detail;

import java.io.Serializable;

import javax.persistence.*;

import com.capstoneproject.user_management.bill.bill;

@Entity
@Table(name =  "bill_detail")
public class bill_detail implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_billdetail", nullable = false)
    private int id_billdetail;

    @Column(name = "id_product", nullable = false)
    private int id_product;

    @Column(name = "quantity_product", nullable = false)
    private int quantity_product;

    @ManyToOne
    @JoinColumn(name = "id_bill") // thông qua khóa ngoại id_bill
    private bill bill_id;

    public int getId_billdetail() {
        return id_billdetail;
    }

    public void setId_billdetail(int id_billdetail) {
        this.id_billdetail = id_billdetail;
    }

    public int getId_product() {
        return id_product;
    }

    public void setId_product(int id_product) {
        this.id_product = id_product;
    }

    public int getQuantity_product() {
        return quantity_product;
    }

    public void setQuantity_product(int quantity_product) {
        this.quantity_product = quantity_product;
    }

    public bill getBill_id() {
        return bill_id;
    }

    public void setBill_id(bill bill_id) {
        this.bill_id = bill_id;
    }

}
