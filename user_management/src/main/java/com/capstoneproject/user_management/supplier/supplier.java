package com.capstoneproject.user_management.supplier;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.*;

import com.capstoneproject.user_management.Product.product;

@Entity
@Table(name =  "supplier")
public class supplier implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_supplier", nullable = false)
    private int id_supplier;

    @Column(name = "name_supplier", length = 200, nullable = false)
    private String name_supplier;

    @Column(name = "email_supplier", length = 100, nullable = false)
    private String email_supplier;

    @Column(name = "hotline_supplier", length = 20, nullable = false)
    private String hotline_supplier;

    @Column(name = "description_supplier", length = Integer.MAX_VALUE, nullable = false)
    private String description_supplier;

    @OneToMany(mappedBy = "id_supplier", cascade = CascadeType.ALL) // Quan hệ 1-n với đối tượng ở dưới (product) (1 nhà cung cấp có nhiều sản phẩm)
    // MapopedBy trỏ tới tên biến id_supplier ở trong product.
    private Collection<product> persons;

    public int getId_supplier() {
        return id_supplier;
    }

    public void setId_supplier(int id_supplier) {
        this.id_supplier = id_supplier;
    }

    public String getName_supplier() {
        return name_supplier;
    }

    public void setName_supplier(String name_supplier) {
        this.name_supplier = name_supplier;
    }

    public String getEmail_supplier() {
        return email_supplier;
    }

    public void setEmail_supplier(String email_supplier) {
        this.email_supplier = email_supplier;
    }

    public String getHotline_supplier() {
        return hotline_supplier;
    }

    public void setHotline_supplier(String hotline_supplier) {
        this.hotline_supplier = hotline_supplier;
    }

    public String getDescription_supplier() {
        return description_supplier;
    }

    public void setDescription_supplier(String description_supplier) {
        this.description_supplier = description_supplier;
    }

    public Collection<product> getPersons() {
        return persons;
    }

    public void setPersons(Collection<product> persons) {
        this.persons = persons;
    }

    
}
