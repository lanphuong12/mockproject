package com.capstoneproject.user_management.price_product;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import com.capstoneproject.user_management.Product.product;

@Entity
@Table(name =  "price_product")
public class price_product implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_price", nullable = false)
    private int id_price;

    @Column(name = "id_product", nullable = false)
    private int id_product;

    @Column(name = "discount", nullable = true)
    private int discount;

    @Column(name = "product_price", nullable = false)
    private int product_price;

    @Column(name = "current_price_product", nullable = false)
    private int current_price_product;

    @Temporal(TemporalType.DATE)
    @Column(name = "start_date", nullable = true)
    private Date start_date;

    @Temporal(TemporalType.DATE)
    @Column(name = "end_date", nullable = true)
    private Date end_date;

    @OneToOne(mappedBy = "price")
    private product current_price;

    public int getId_price() {
        return id_price;
    }

    public void setId_price(int id_price) {
        this.id_price = id_price;
    }

    public int getId_product() {
        return id_product;
    }

    public void setId_product(int id_product) {
        this.id_product = id_product;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public int getCurrent_price_product() {
        return current_price_product;
    }

    public void setCurrent_price_product(int current_price_product) {
        this.current_price_product = current_price_product;
    }

    public Date getStart_date() {
        return start_date;
    }

    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    public Date getEnd_date() {
        return end_date;
    }

    public void setEnd_date(Date end_date) {
        this.end_date = end_date;
    }

    public int getProduct_price() {
        return product_price;
    }

    public void setProduct_price(int product_price) {
        this.product_price = product_price;
    }

    
}
