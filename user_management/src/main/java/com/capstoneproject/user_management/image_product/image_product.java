package com.capstoneproject.user_management.image_product;

import java.io.Serializable;

import javax.persistence.*;

import com.capstoneproject.user_management.Product.product;

@Entity
@Table(name =  "image_product")
public class image_product implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_image", nullable = false)
    private int id_image;

    @Column(name = "path_image", length = Integer.MAX_VALUE, nullable = false)
    private String path_image;

    @ManyToOne
    @JoinColumn(name = "id_product") // thông qua khóa ngoại id_product
    private product image;

    public int getId_image() {
        return id_image;
    }

    public void setId_image(int id_image) {
        this.id_image = id_image;
    }

    public String getPath_image() {
        return path_image;
    }

    public void setPath_image(String path_image) {
        this.path_image = path_image;
    }

    
}
