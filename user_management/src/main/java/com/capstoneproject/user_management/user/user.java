package com.capstoneproject.user_management.user;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.*;

import com.capstoneproject.user_management.bill.bill;

@Entity
@Table(name =  "user")
public class user implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_user", nullable = false)
    private int id_user;

    @Column(name = "username", length = 200, nullable = false)
    private String username;

    @Column(name = "pass", length = 200, nullable = false)
    private String pass;

    @Column(name = "name_user", length = 200, nullable = false)
    private String name_user;

    @Column(name = "address_user", length = Integer.MAX_VALUE, nullable = false)
    private String address_user;

    @Column(name = "email_user", length = 200, nullable = false)
    private String email_user;

    @Column(name = "phone_user", length = 200, nullable = false)
    private String phone_user;

    @Column(name = "avatar_user", length = Integer.MAX_VALUE, nullable = false)
    private String avatar_user;

    @OneToMany(mappedBy = "id_user", cascade = CascadeType.ALL) // Quan hệ 1-n với đối tượng ở dưới (bill) (1 người dùng có nhiều hóa đơn)
    // MapopedBy trỏ tới tên biến id_user ở trong bill.
    private Collection<bill> bills;

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getName_user() {
        return name_user;
    }

    public void setName_user(String name_user) {
        this.name_user = name_user;
    }

    public String getAddress_user() {
        return address_user;
    }

    public void setAddress_user(String address_user) {
        this.address_user = address_user;
    }

    public String getEmail_user() {
        return email_user;
    }

    public void setEmail_user(String email_user) {
        this.email_user = email_user;
    }

    public String getPhone_user() {
        return phone_user;
    }

    public void setPhone_user(String phone_user) {
        this.phone_user = phone_user;
    }

    public String getAvatar_user() {
        return avatar_user;
    }

    public void setAvatar_user(String avatar_user) {
        this.avatar_user = avatar_user;
    }

    
}
