package com.capstoneproject.user_management.bill;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.*;

import com.capstoneproject.user_management.bill_detail.bill_detail;
import com.capstoneproject.user_management.user.user;

@Entity
@Table(name =  "bill")
public class bill implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_bill", nullable = false)
    private int id_bill;

    @ManyToOne
    @JoinColumn(name = "id_user") // thông qua khóa ngoại id_supplier
    private user id_user;

    @Column(name = "quantity_product", nullable = false)
    private int quantity_product; // số lượng sản phẩm mua

    @Column(name = "total_bill", nullable = false)
    private int total_bill; // tổng hóa đơn chưa giảm giá

    @Column(name = "id_voucher", nullable = true)
    private int id_voucher;

    @Column(name = "payment_bill", nullable = false)
    private int payment_bill; // tổng tiền sau khi áp mã giảm giá

    @OneToMany(mappedBy = "bill_id", cascade = CascadeType.ALL) // Quan hệ 1-n với đối tượng ở dưới (bill_detail) (1 hóa đơn có nhiều chi tiết hóa đơn)
    // MapopedBy trỏ tới tên biến bill_id ở trong bill_detail.
    private Collection<bill_detail> bill_id;

    public int getId_bill() {
        return id_bill;
    }

    public void setId_bill(int id_bill) {
        this.id_bill = id_bill;
    }

    public int getQuantity_product() {
        return quantity_product;
    }

    public void setQuantity_product(int quantity_product) {
        this.quantity_product = quantity_product;
    }

    public int getTotal_bill() {
        return total_bill;
    }

    public void setTotal_bill(int total_bill) {
        this.total_bill = total_bill;
    }

    public int getId_voucher() {
        return id_voucher;
    }

    public void setId_voucher(int id_voucher) {
        this.id_voucher = id_voucher;
    }

    public int getPayment_bill() {
        return payment_bill;
    }

    public void setPayment_bill(int payment_bill) {
        this.payment_bill = payment_bill;
    }

    public user getId_user() {
        return id_user;
    }

    public void setId_user(user id_user) {
        this.id_user = id_user;
    }

}
